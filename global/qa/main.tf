resource "null_resource" "helloWorld" {
  count = 1

  provisioner "local-exec" {
    command = "echo helloworld ${count.index}"
  }
}

output "local-exec" {
  value = "${null_resource.helloWorld.id}"
}
